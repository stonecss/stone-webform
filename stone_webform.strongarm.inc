<?php
/**
 * @file
 * stone_webform.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function stone_webform_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_node_extended_webform';
  $strongarm->value = 1;
  $export['i18n_node_extended_webform'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_node_options_webform';
  $strongarm->value = array();
  $export['i18n_node_options_webform'] = $strongarm;

  return $export;
}
